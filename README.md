# Scripts to set up Selenium Hub / Node with dependencies on various platforms

## CentOS
Superuser permissions required (`sudo`).

### Selenium Hub
Download `create-selenium-hub.sh` to user home folder (`~`) on the machine hosting Selenium Hub. Update `SELENIUM_VERSION` environment variable in accordance with the latest Selenium version available.

You might need to grant executable permissions otherwise the script may not run:
```
$ chmod +x create-selenium-hub.sh
```

Then run the script:
```
$ ./create-selenium-hub.sh
```

At the end of the execution `selenium-server-standalone-x.x.jar` should start with the Hub role. `hub_config.json` can be found in `~/.selenium` directory.

### Selenium Node

#### Install Google Chrome browser
Download `install-google-chrome.sh` (developed by Intoli, LLC) to user home folder (`~`) on the machine hosting Selenium Node.

You might need to grant executable permissions otherwise the script may not run:
```
$ chmod +x install-google-chrome.sh
```

Then run the script:
```
$ ./install-google-chrome.sh
```

#### Start up the Node
Download `create-selenium-node.sh` to user home folder (`~`) on the machine hosting Selenium Node. Update `SELENIUM_VERSION` environment variable in accordance with the latest Selenium version available and `HUB_IP` to define the IP address of the machine running Selenium Hub.

You might need to grant executable permissions otherwise the script may not run:
```
$ chmod +x create-selenium-node.sh
```

Then run the script:
```
$ ./create-selenium-node.sh
```

At the end of the execution `selenium-server-standalone-x.x.jar` should start with the Node role. `node_config.json` can be found in `~/.selenium` directory.

# Contribution
Feel free to restructure the project and add new stuff ;)