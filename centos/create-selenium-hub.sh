#!/bin/bash

SELENIUM_VERSION="3.14"


# Update yum
sudo yum -y update


# Install tools
sudo yum -y install unzip vi wget curl


# Install Java
sudo yum -y install java-1.8.0-openjdk


# Create base directory for Selenium
mkdir ~/.selenium
cd ~/.selenium


# Copy SBXCapabilityMatcher.jar to the base directory
cp ~/SBXCapabilityMatcher.jar ~/.selenium/SBXCapabilityMatcher.jar


# Create Hub config
cat << EOF > hub_config.json
{
  "host": "0.0.0.0",
  "maxSessions": 4,
  "port": 4444,
  "cleanupCycle": 5000,
  "timeout": 30000,
  "newSessionWaitTimeout": -1,
  "servlets": [],
  "prioritizer": null,
  "capabilityMatcher": "org.openqa.grid.internal.utils.DefaultCapabilityMatcher",
  "throwOnCapabilityNotPresent": true,
  "nodePolling": 60000
}
EOF


# Download selenium-server-standalone binary
wget http://selenium-release.storage.googleapis.com/$SELENIUM_VERSION/selenium-server-standalone-$SELENIUM_VERSION.0.jar


# Start Selenium Hub
nohup java -jar selenium-server-standalone-$SELENIUM_VERSION.0.jar -role hub -hubConfig hub_config.json &
echo "Selenium Hub setup complete"
