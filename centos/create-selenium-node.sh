#!/bin/bash

HUB_IP="10.10.10.10"
SELENIUM_VERSION="3.14"


# Update yum
sudo yum -y update


# Install tools
sudo yum -y install unzip vi wget curl


# Install Java
sudo yum -y install java-1.8.0-openjdk


# Create base directory for Selenium
mkdir ~/.selenium
cd ~/.selenium


# Create Node config
node_ip=$(/sbin/ifconfig eth0 | grep 'inet' | awk '{print $2}' | awk 'NR==1' | cut -d':' -f2)
cat << EOF > node_config.json
{
  "capabilities":
  [
    {
      "browserName": "chrome",
      "maxInstances": 4,
      "seleniumProtocol": "WebDriver"
    }
  ],
  "proxy": "org.openqa.grid.selenium.proxy.DefaultRemoteProxy",
  "maxSession": 4,
  "timeout": 120,
  "host" : "$node_ip",
  "port": 5555,
  "register": true,
  "registerCycle": 5000,
  "hub": "http://$HUB_IP:4444",
  "nodeStatusCheckTimeout": 5000,
  "nodePolling": 5000,
  "unregisterIfStillDownAfter": 60000,
  "downPollingLimit": 2,
  "debug": false,
  "servlets" : [],
  "withoutServlets": [],
  "custom": {}
}
EOF


# Download chromedriver and selenium-server-standalone binary
chromedriver_latest=$(curl https://chromedriver.storage.googleapis.com/LATEST_RELEASE)
wget https://chromedriver.storage.googleapis.com/$chromedriver_latest/chromedriver_linux64.zip
unzip -o chromedriver_linux64.zip
rm chromedriver_linux64.zip

wget http://selenium-release.storage.googleapis.com/$SELENIUM_VERSION/selenium-server-standalone-$SELENIUM_VERSION.0.jar


# Install Xvfb required for running browsers
sudo yum -y install Xvfb
Xvfb :99 &
export DISPLAY=:99


# Start Selenium Node
nohup java -jar -Dwebdriver.chrome.driver=chromedriver selenium-server-standalone-$SELENIUM_VERSION.0.jar -role node -nodeConfig node_config.json &
echo "Selenium Node setup complete"
